import { useEffect, useState } from "react";
import { getPeople } from './api';

const App = () => {
  //function App() {
  const [page, setPage] = useState(1);
  const [datos, setDatos] = useState([]);

  const [selectedItem, setSelectedItem] = useState('');
  const [apiData, setApiData] = useState(null);

  useEffect(() => {
    getPeople(page).then((res) => {
      const newDatos = res.data.resultados;
      setDatos(newDatos);
      console.log(newDatos);



    });

    if (selectedItem) {
      // Simulando la solicitud a una API (reemplaza con tu lógica y URL real)
      fetch(`https://cima.aemps.es/cima/rest/medicamento?nregistro=${selectedItem}`)
        .then(response => response.json())
        .then(data => setApiData(data))
        .catch(error => console.error('Error al obtener datos de la API:', error));
    }


  

}, [selectedItem]);

  // Función para manejar el cambio en el select
  const handleSelectChange = (event) => {
    setSelectedItem(event.target.value);
  };

return (
  <div id="principal">
    Resultados en la consola
    <div id="navegacion">


      <ul>
        {datos.map((number) => (
          <li id={number.nregistro}>{number.nombre}</li>
        ))}
      </ul>
      <select>
        {datos.map((number) => (
          <option value={number.nregistro}>{number.nombre}</option>
        ))}
      </select>
      <select value={selectedItem} onChange={handleSelectChange}  size="8" >
        {datos.map((number) => (
          <option value={number.nregistro}>{number.nombre}</option>
        ))}
      </select>
    </div>
    <div id="detalle">
      <div>
        {apiData && (
          <>
            <h2>Datos para el numero de registro {selectedItem}</h2>
            <p><b>Nombre:</b> {apiData.nombre}</p>
            <p><b>Principio activo:</b> {apiData.pactivos}</p>
            <p><b>Uso:</b> {apiData.cpresc}</p>
            <p><b>Laboratorio:</b> {apiData.labtitular}</p>
            <p><b>Descripción:</b> {apiData.atcs[1].nombre}</p>
            <p><b>Formato:</b> {apiData.formaFarmaceuticaSimplificada.nombre}</p>
            <p><b>Via de administración:</b> {apiData.viasAdministracion[0].nombre}</p>
            <p><b>Principio activo:</b></p>
            <ul>
            {apiData.principiosActivos.map((elemento,indice)=>
                <li>{elemento.nombre} ({elemento.cantidad}{elemento.unidad})</li>
            )}
            </ul>
            
           
            <img src={apiData.fotos[0].url}/>
            <img src={apiData.fotos[1].url}/>
         
         
            {/* Agrega más campos según sea necesario */}
          </>
        )}
      </div>

    </div>

  </div>
);
}

export default App;
